#!/bin/bash
# displays battery remaining in hours
if [ "$1" = "-d" ]; then
  upower -i $(upower -e | grep BAT)
else
  upower -i $(upower -e | grep BAT) | grep 'time to empty' | sed -r 's/time to empty:| |hours//g'
fi
