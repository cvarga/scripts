#!/bin/bash
#  set environmental variables listed in .env
#  run with ". set_env.sh"
lines=$(cat .env)
for line in $lines ; do
    export $line
done
